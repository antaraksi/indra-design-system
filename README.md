## To add to your project:

```
# this will add the latest official release
yarn add indra-design-system
```

### To use an older version, add pin the npm version you want to your package.json dependencies:

```
...
"dependencies": {
  "indra-design-system": "^0.0.0"
}
...
```

### To generate static asset files to use in a stand-alone HTML doc, run this command:

```
yarn build:static
```

Then use `<script/>` and `<link/>` tags to include to the js and css files in the `static/system` directory in your HTML. Then include any indra components in a wrapper with `class="indra"`.

## To contribute to indra:

- You will need Node v10.16.0+
- If you use a NVM, run `nvm install` or `nvm use` if you already know you have Node v10.16.0

```
yarn install
```

### To start development server:

```
yarn start
```

### To run all unit tests:

```
yarn test
```

### To run a single test:

```
yarn unit mytest.spec.js
```

### To develop indra locally as a dependency in a project

You might want to develop indra while developing on a project that uses indra as a dependency. For example, you might want to develop indra components while developing the Approvals app. NOTE: You will need to have both indra and the project using it as a dependency running on your machine.

1. Install [yalc](https://github.com/whitecolor/yalc)
1. In the indra root directory, run `yarn build:system && yalc publish`
1. In the Project (e.g., Approvals) root directory, run `yalc add indra-design-system`
1. Each time that you are ready to view the indra work in progress, run the last 2 steps:
   1. In the indra root directory, run `yarn build:system && yalc publish`
   1. In the Project (e.g., Approvals) root directory, run `yalc update`

## To deploy [docs](https://pulibrary.github.io/indra/docs/#/Getting%20Started):

```
# make sure git status is clean with no uncommitted modified files
yarn run deploy
```

## To publish a new version to npm:

1.  Pull latest master with merged changes.
2.  Run `yarn release`
3.  :tada:
